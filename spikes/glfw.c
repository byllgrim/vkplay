#include <stdio.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>


enum {
  WIN_WIDTH  = 800,
  WIN_HEIGHT = 600,
};


int
main(void)
{
  GLFWwindow *win;

  glfwInit();
  win = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "mytitle", 0, 0);
  while (!glfwWindowShouldClose(win)) {
    glfwPollEvents();
  }

  glfwTerminate();
  return EXIT_SUCCESS;
}
