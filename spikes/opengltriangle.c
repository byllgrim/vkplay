// Following "learnopengl.com" and "opengl-tutorial.org"

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


const char *vertexShaderSource =
  "#version 330 core                                      \n"
  "layout (location = 0) in vec3 mypos;                   \n"
  "void main() {                                          \n"
  "   gl_Position = vec4(mypos.x, mypos.y, mypos.z, 1.0); \n"
  "}                                                      \n\0";

const char *fragmentShaderSource =
  "#version 330 core               \n"
  "out vec4 FragColor;             \n"
  "void main() {                   \n"
  "  FragColor = vec4(1, 0, 0, 1); \n"
  "}                               \n\0";


int
main(void)
{
  GLfloat  vertexBufferData[] = {
   -1.0, -1.0, 0.0,
    1.0, -1.0, 0.0,
    0.0,  1.0, 0.0,
  };
  int         res;
  GLFWwindow *window;
  GLuint      vertexArrays;
  GLuint      vertexBuffer;
  GLuint      vertexShader;
  GLuint      fragmentShader;
  GLuint      shaderProgram;

  printf("hello world\n");

  // Unknown magic for "core profile"
  glewExperimental = 1;

  // Glfw
  res = glfwInit();
  if (!res) printf("error: glfwInit failed\n");
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  window = glfwCreateWindow(800, 600, "", 0, 0);
  if (!window) printf("eror: glfwCreateWindow failed\n");
  glfwMakeContextCurrent(window);

  // Glew
  glewExperimental = 1;
  res = glewInit();
  if (res != GLEW_OK) printf("error: glewInit failed\n");

  // Vertex Array Object (VAO)
  glGenVertexArrays(1, &vertexArrays);
  glBindVertexArray(vertexArrays);

  // Vertex Buffer
  glGenBuffers(1, &vertexBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

  // Attribute Something
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

  // Vertex Shader
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderSource, 0);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &res);
  if (!res) printf("error: vertex shader failed\n");

  // Fragment Shader
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderSource, 0);
  glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &res);
  if (!res) printf("error: fragment shader failed\n");

  // Shader Program
  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &res);
  if (!res) printf("error: shader program failed\n");

  // Clear
  glClearColor(0, 0, 0.5, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Draw
  glUseProgram(shaderProgram);
  glDrawArrays(GL_TRIANGLES, 0, 3);
  glDisableVertexAttribArray(0);
  glfwSwapBuffers(window);

  // Funny main loop
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
  }

  return EXIT_SUCCESS;
}
