#include <stdio.h>
#include <stdlib.h>

#include <vulkan/vulkan.h>

int
main(void)
{
  VkResult              res                  = {0};
  VkInstanceCreateInfo  instanceCreateInfo   = {0};
  VkInstance            instance             = {0};
  const char* const     enabledLayerNames[1] = {"VK_LAYER_KHRONOS_validation"};

  printf("hello validation\n");

  //instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instanceCreateInfo.enabledLayerCount = 1;
  instanceCreateInfo.ppEnabledLayerNames = enabledLayerNames;

  res = vkCreateInstance(&instanceCreateInfo, 0, &instance);
  switch (res) {
    case VK_SUCCESS:
      break;
    case VK_ERROR_INITIALIZATION_FAILED:
      fprintf(stderr, "error: vkCreateInstance init failed\n");
      break;
    case VK_ERROR_LAYER_NOT_PRESENT:
      fprintf(stderr, "error: vkCreateInstance layer not present\n");
      break;
    default:
      fprintf(stderr, "error: vkCreateInstance unknown failure\n");
  }

  return EXIT_SUCCESS;
}
