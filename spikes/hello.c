#include <stdio.h>
#include <stdlib.h>
#include <vulkan/vulkan.h>

enum {
  DEV_COUNT = 8,
};

int
main(void)
{
  const VkInstanceCreateInfo  createInfo                 = {0};
  VkInstance                  instance                   = {0};
  uint32_t                    physicalDeviceCount        = DEV_COUNT;
  VkPhysicalDevice            physicalDevices[DEV_COUNT] = {0};
  VkPhysicalDeviceProperties  properties                 = {0};

  vkCreateInstance              (&createInfo, 0, &instance);
  vkEnumeratePhysicalDevices    (instance, &physicalDeviceCount, physicalDevices);
  vkGetPhysicalDeviceProperties (physicalDevices[0], &properties);

  printf ("physicalDeviceCount: %d\n", physicalDeviceCount);
  printf (
    "apiVersion: %d.%d.%d\n",
    VK_API_VERSION_MAJOR (properties.apiVersion),
    VK_API_VERSION_MINOR (properties.apiVersion),
    VK_API_VERSION_PATCH (properties.apiVersion)
  );
  printf ("driverVersion: %d\n", properties.driverVersion);
  printf ("vendorID: 0x%x\n", properties.vendorID);
  printf ("deviceName: '%s'\n", properties.deviceName);

  return EXIT_SUCCESS;
}
