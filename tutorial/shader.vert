#version 450

// Borrowed from "vulkan-tutorial.com"

layout(location = 0) out vec3 fragColor;

vec2 screen_positions[3] = vec2[](
    vec2(  0.0, -0.5 ),
    vec2(  0.5,  0.5 ),
    vec2( -0.5,  0.5 )
);

vec3 colors[3] = vec3[](
    vec3( 1.0, 0.0, 0.0 ),
    vec3( 0.0, 1.0, 0.0 ),
    vec3( 0.0, 0.0, 1.0 )
);

void main() {
    vec2   screen_position = screen_positions[ gl_VertexIndex ];
    float  z               = 0.0;
    float  w               = 1.0;

    gl_Position = vec4( screen_position, z, w );
    fragColor   = colors[ gl_VertexIndex ];
}
