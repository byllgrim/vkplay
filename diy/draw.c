// NB! Will fail in mysterious ways if libs and drivers aren't installed.

#include <sys/stat.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <vulkan/vulkan.h>

#include <GLFW/glfw3.h>

#define  MIN(A, B)  ((A) < (B) ? (A) : (B))

enum {
  COM_BUF_COUNT = 1,
  PHY_DEV_COUNT = 1,
  QUE_INF_COUNT = 1,
  QUE_COUNT     = 1,
  CRE_INF_COUNT = 1,
  STA_COUNT     = 2,
  EXT_COUNT     = 3,
  TIMEOUT       = UINT64_MAX,
};

void readFile(char *codeFilename, uint32_t **code, size_t* codeSize) {
  struct stat  statbuf;
  int          fd;

  stat(codeFilename, &statbuf);

  *codeSize = statbuf.st_size;
  *code     = calloc(65536, 1);
  fd        = open(codeFilename, 0, O_RDONLY);

  read(fd, *code, *codeSize);
}

VkBool32  messengerCallback (
  VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity,
  VkDebugUtilsMessageTypeFlagsEXT                  messageTypes,
  const VkDebugUtilsMessengerCallbackDataEXT*      pCallbackData,
  void*                                            pUserData
) {
  (void)messageSeverity;
  (void)messageTypes;
  (void)pUserData;

  fprintf(stderr, "message: '%s'\n", pCallbackData->pMessage);

  return  VK_FALSE;
}

void createSwapchain(
  VkSurfaceKHR              surface,
  VkSurfaceCapabilitiesKHR  surfaceCapabilities,
  VkDevice                  device,
  VkSwapchainKHR           *pSwapchains
  )
{
  VkSwapchainCreateInfoKHR  swapchainCreateInfo = {0};
  VkResult                  result              = {0};

  swapchainCreateInfo.sType                 = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
//swapchainCreateInfo.pNext                 = ;
//swapchainCreateInfo.flags                 = ;
  swapchainCreateInfo.surface               = surface;
  swapchainCreateInfo.minImageCount         = surfaceCapabilities.minImageCount;
  swapchainCreateInfo.imageFormat           = VK_FORMAT_B8G8R8A8_SRGB;  // TODO surfaceFormat.format;
//swapchainCreateInfo.imageColorSpace       = ;  // TODO ?
  swapchainCreateInfo.imageExtent           = (VkExtent2D){800, 600};  // TODO "pipeline"
  swapchainCreateInfo.imageArrayLayers      = 1;
  swapchainCreateInfo.imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
//swapchainCreateInfo.imageSharingMode      = ;  // TODO ?
//swapchainCreateInfo.queueFamilyIndexCount = ;  // TODO ?
//swapchainCreateInfo.pQueueFamilyIndices   = ;  // TODO ?
  swapchainCreateInfo.preTransform          = surfaceCapabilities.currentTransform;
  swapchainCreateInfo.compositeAlpha        = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
//swapchainCreateInfo.presentMode           = ;  // TODO ?
//swapchainCreateInfo.clipped               = ;  // TODO ?
//swapchainCreateInfo.oldSwapchain          = ;

// TODO glfwPollEvents() problem

  result = vkCreateSwapchainKHR(device, &swapchainCreateInfo, 0, pSwapchains);
  if (result != VK_SUCCESS) fprintf(stderr, "error: swapchain creation failed, %d\n", result);
}

void createRenderPass(
  VkDevice      device,
  VkRenderPass *pRenderPass
  )
{
  VkAttachmentDescription  attachmentDescription = {0};
  VkSubpassDescription     subpassDescription    = {0};
  VkRenderPassCreateInfo   renderPassCreateInfo  = {0};
  VkResult                 result                = {0};

//attachmentDescription.flags          = ;
  attachmentDescription.format         = VK_FORMAT_B8G8R8A8_SRGB;  // TODO surfaceFormat.format;
  attachmentDescription.samples        = VK_SAMPLE_COUNT_1_BIT;
  attachmentDescription.loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attachmentDescription.storeOp        = VK_ATTACHMENT_STORE_OP_STORE;
  attachmentDescription.stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attachmentDescription.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
  attachmentDescription.finalLayout    = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

//subpassDescription.flags                   = ;
  subpassDescription.pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
//subpassDescription.inputAttachmentCount    = ;
//subpassDescription.pInputAttachments       = ;
//subpassDescription.colorAttachmentCount    = 1;  // TODO
//subpassDescription.pColorAttachments       = ;  // TODO
//subpassDescription.pResolveAttachments     = ;
//subpassDescription.pDepthStencilAttachment = ;
//subpassDescription.preserveAttachmentCount = ;
//subpassDescription.pPreserveAttachments    = ;

  renderPassCreateInfo.sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
//renderPassCreateInfo.pNext           = ;
//renderPassCreateInfo.flags           = ;
  renderPassCreateInfo.attachmentCount = 1;
  renderPassCreateInfo.pAttachments    = &attachmentDescription;
  renderPassCreateInfo.subpassCount    = 1;
  renderPassCreateInfo.pSubpasses      = &subpassDescription;
//renderPassCreateInfo.dependencyCount = 1;  // TODO
//renderPassCreateInfo.pDependencies   = ;  // TODO

  result = vkCreateRenderPass(device, &renderPassCreateInfo, 0, pRenderPass);
  if (result != VK_SUCCESS) fprintf(stderr, "error: create render pass failed\n");
}

void createImageView(
  VkImage      image,
  VkDevice     device,
  VkImageView *pImageView
  )
{
  VkImageViewCreateInfo  imageViewCreateInfo = {0};
  VkResult               result              = {0};

  imageViewCreateInfo.sType    = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
//imageViewCreateInfo.pNext    = ;
//imageViewCreateInfo.flags    = ;
  imageViewCreateInfo.image    = image;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format   = VK_FORMAT_B8G8R8A8_SRGB;  // TODO surfaceFormat.format;

  imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

  imageViewCreateInfo.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
  imageViewCreateInfo.subresourceRange.baseMipLevel   = 0;
  imageViewCreateInfo.subresourceRange.levelCount     = 1;
  imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
  imageViewCreateInfo.subresourceRange.layerCount     = 1;

  result = vkCreateImageView(device, &imageViewCreateInfo, 0, pImageView);
  if (result != VK_SUCCESS) fprintf(stderr, "image view failed to create, %d\n", result);
}

void createGraphicsPipelines(
  VkShaderModule    vertexShaderModule,
  VkPipelineLayout  pipelineLayout,
  VkRenderPass      renderPass,
  VkDevice          device,
  VkPipeline       *pPipelines
  )
{
  VkPipelineShaderStageCreateInfo         vertexShaderStageCreateInfo                = {0};
  VkPipelineShaderStageCreateInfo         fragmentShaderStageCreateInfo              = {0};
  VkPipelineShaderStageCreateInfo         pipelineShaderStageCreateInfos[STA_COUNT]  = {0};
  VkPipelineRasterizationStateCreateInfo  rasterizationStateCreateInfo               = {0};
  VkPipelineViewportStateCreateInfo       viewportStateCreateInfo                    = {0};
  VkPipelineMultisampleStateCreateInfo    multisampleStateCreateInfo                 = {0};
  VkPipelineInputAssemblyStateCreateInfo  inputAssemblyStateCreateInfo               = {0};
  VkPipelineVertexInputStateCreateInfo    vertexInputStateCreateInfo                 = {0};
  VkPipelineColorBlendStateCreateInfo     pipelineColorBlendStateCreateInfo          = {0};
  VkGraphicsPipelineCreateInfo            graphicsPipelineCreateInfos[CRE_INF_COUNT] = {0};
  VkRect2D                                scissor                                    = {0};
  VkViewport                              viewport                                   = {0};
  VkPipelineColorBlendAttachmentState     pipelineColorBlendAttachmentState          = {0};
  VkResult                                result                                     = {0};
  uint32_t                               createInfoCount                             = CRE_INF_COUNT;

  vertexShaderStageCreateInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  vertexShaderStageCreateInfo.stage  = VK_SHADER_STAGE_VERTEX_BIT;
  vertexShaderStageCreateInfo.module = vertexShaderModule;
  vertexShaderStageCreateInfo.pName  = "main";

  fragmentShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  fragmentShaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;

  pipelineShaderStageCreateInfos[0] = vertexShaderStageCreateInfo;
  pipelineShaderStageCreateInfos[1] = fragmentShaderStageCreateInfo;

  rasterizationStateCreateInfo.sType       = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizationStateCreateInfo.lineWidth   = 1;
  rasterizationStateCreateInfo.frontFace   = VK_FRONT_FACE_CLOCKWISE;
  rasterizationStateCreateInfo.cullMode    = VK_CULL_MODE_BACK_BIT;
  rasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;

  scissor.extent = (VkExtent2D){800, 600};  // TODO "swapchain"

  viewport.width    = 800;  // TODO don't hardcode
  viewport.height   = 600;
  viewport.maxDepth = 1;

  viewportStateCreateInfo.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewportStateCreateInfo.viewportCount = 1;  // PRT_COUNT;
  viewportStateCreateInfo.scissorCount  = 1;  // SCI_COUNT;
  viewportStateCreateInfo.pViewports    = &viewport;
  viewportStateCreateInfo.pScissors     = &scissor;

  multisampleStateCreateInfo.sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
  multisampleStateCreateInfo.minSampleShading     = 1;

  inputAssemblyStateCreateInfo.sType    = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  inputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

  vertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

  //                               .blendEnable         = Z
  //                               .srcColorBlendFactor = Z
  //                               .dstColorBlendFactor = Z
  //                               .colorBlendOp        = Z
  //                               .srcAlphaBlendFactor = Z
  //                               .dstAlphaBlendFactor = Z
  //                               .alphaBlendOp        = Z
  pipelineColorBlendAttachmentState.colorWriteMask      =
    VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;;

  pipelineColorBlendStateCreateInfo.sType           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  //                               .pNext           = Z
  //                               .flags           = Z
  //                               .logicOpEnable   = TODO ?
  //                               .logicOp         = Z
  pipelineColorBlendStateCreateInfo.attachmentCount = 1;
  pipelineColorBlendStateCreateInfo.pAttachments    = &pipelineColorBlendAttachmentState;
  //                               .blendConstants  = Z

  graphicsPipelineCreateInfos[0].sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  //pNext
  //flags
  graphicsPipelineCreateInfos[0].stageCount          = 1;  // 2;  // STA_COUNT;
  graphicsPipelineCreateInfos[0].pStages             = pipelineShaderStageCreateInfos;
  graphicsPipelineCreateInfos[0].pVertexInputState   = &vertexInputStateCreateInfo;
  graphicsPipelineCreateInfos[0].pInputAssemblyState = &inputAssemblyStateCreateInfo;
  //pTessellationState TODO ?
  graphicsPipelineCreateInfos[0].pViewportState      = &viewportStateCreateInfo;
  graphicsPipelineCreateInfos[0].pRasterizationState = &rasterizationStateCreateInfo;
  graphicsPipelineCreateInfos[0].pMultisampleState   = &multisampleStateCreateInfo;
  //pDepthStencilState TODO ?
  graphicsPipelineCreateInfos[0].pColorBlendState    = &pipelineColorBlendStateCreateInfo;
  //pDynamicState
  graphicsPipelineCreateInfos[0].layout              = pipelineLayout;
  graphicsPipelineCreateInfos[0].renderPass          = renderPass;
  //subpass TODO ?
  //basePipelineHandle TODO ?
  graphicsPipelineCreateInfos[0].basePipelineIndex   = -1;

  result = vkCreateGraphicsPipelines(device, 0, createInfoCount, graphicsPipelineCreateInfos, 0, pPipelines);
  if (result != VK_SUCCESS) fprintf(stderr, "create graphics pipeline failed\n");
}

void createWindow(GLFWwindow **ppWindow) {
  const char **glfwExtensionsNames = {0};
  uint32_t     glfwExtensionsCount = {0};

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

  *ppWindow = glfwCreateWindow(800, 600, "", 0, 0);

  (void)glfwExtensionsNames;
  (void)glfwExtensionsCount;
  /*
  glfwExtensionsNames = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);
  printf("glfw vulkan supported = %d\n", glfwVulkanSupported());
  printf("glfw ext count = %d\n", glfwExtensionsCount);
  printf("glfw ext names = 0x%x\n", glfwExtensionsNames);
  for (int i = 0; i < glfwExtensionsCount; i++) {
    printf("%d: '%s'\n", i, glfwExtensionsNames[i]);
  }
  */
}

void createInstance(
  VkInstance *pInstance
  )
{
  VkInstanceCreateInfo  instanceCreateInfo               = {0};
  const char * const    enabledLayerNames[]              = {"VK_LAYER_KHRONOS_validation"};
  VkResult              result                           = {0};
  const char * const    enabledExtensionNames[EXT_COUNT] = {
    VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
    "VK_KHR_surface",  // TODO VK_KHR_SURFACE_EXTENSION_NAME,
    "VK_KHR_xcb_surface",  // TODO VK_KHR_XCB_SURFACE_EXTENSION_NAME,
  };

  instanceCreateInfo.sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
//instanceCreateInfo.pNext                   = ;
//instanceCreateInfo.flags                   = ;
//instanceCreateInfo.pApplicationInfo        = ;
  instanceCreateInfo.enabledLayerCount       = 1;
  instanceCreateInfo.ppEnabledLayerNames     = enabledLayerNames;
  instanceCreateInfo.enabledExtensionCount   = 3; //EXT_COUNT;
  instanceCreateInfo.ppEnabledExtensionNames = enabledExtensionNames;

  result = vkCreateInstance(&instanceCreateInfo, 0, pInstance);
  if (result != VK_SUCCESS) fprintf(stderr, "error: create instance failed, %d\n", result);
}

void createDebugMessenger(
  VkInstance  instance
  )
{
  PFN_vkCreateDebugUtilsMessengerEXT  createDebugUtilsMessenger = {0};
  VkDebugUtilsMessengerCreateInfoEXT  messengerCreateInfo       = {0};
  VkDebugUtilsMessengerEXT            messenger                 = {0};

  createDebugUtilsMessenger =
    (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
  if (!createDebugUtilsMessenger) fprintf(stderr, "error: create debug utils messenger not found\n");

  messengerCreateInfo.sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  messengerCreateInfo.messageSeverity =
    VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  messengerCreateInfo.messageType     =
    VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
    | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
    | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
  messengerCreateInfo.pfnUserCallback = messengerCallback;

  createDebugUtilsMessenger(instance, &messengerCreateInfo, 0, &messenger);
}

void createLogicalDevice(
  VkPhysicalDevice  physicalDevice,
  VkDevice         *pDevice
  )
{
  VkDeviceQueueCreateInfo  deviceQueueCreateInfos[QUE_INF_COUNT] = {0};
  const float              queuePriorities[QUE_COUNT]            = {1};
  VkDeviceCreateInfo       deviceCreateInfo                      = {0};
  const char *             deviceExtensionNames[1]               = {"VK_KHR_swapchain"};

  deviceQueueCreateInfos[0].sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  deviceQueueCreateInfos[0].queueCount       = QUE_COUNT;
  deviceQueueCreateInfos[0].pQueuePriorities = queuePriorities;
  // TODO deviceQueueCreateInfos[0].queueFamilyIndex = ?;
  deviceCreateInfo.sType                     = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  deviceCreateInfo.queueCreateInfoCount      = QUE_INF_COUNT;
  deviceCreateInfo.pQueueCreateInfos         = deviceQueueCreateInfos;
  deviceCreateInfo.enabledExtensionCount     = 1;
  deviceCreateInfo.ppEnabledExtensionNames   = deviceExtensionNames;

  vkCreateDevice(physicalDevice, &deviceCreateInfo, 0, pDevice);
}

void createCommandPool(
  VkDevice       device,
  VkCommandPool *pCommandPool
  )
{
  VkCommandPoolCreateInfo  commandPoolCreateInfo = {0};
  VkResult                 result                = {0};

  commandPoolCreateInfo.sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
//commandPoolCreateInfo.pNext            = ;
  commandPoolCreateInfo.flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
//commandPoolCreateInfo.queueFamilyIndex = ;  // TODO ?

  result = vkCreateCommandPool(device, &commandPoolCreateInfo, 0, pCommandPool);
  if (result != VK_SUCCESS) fprintf(stderr, "error: create command pool failed\n");
}

void createFramebuffer(
  VkRenderPass   renderPass,
  VkImageView   *pImageView,
  VkDevice       device,
  VkFramebuffer *pFramebuffer
  )
{
  VkFramebufferCreateInfo  framebufferCreateInfo = {0};
  VkResult                 result                = {0};

  framebufferCreateInfo.sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  framebufferCreateInfo.renderPass      = renderPass;
  framebufferCreateInfo.attachmentCount = 1;
  framebufferCreateInfo.pAttachments    = pImageView;
  framebufferCreateInfo.width           = 800;
  framebufferCreateInfo.height          = 600;
  framebufferCreateInfo.layers          = 1;

  result = vkCreateFramebuffer(device, &framebufferCreateInfo, 0, pFramebuffer);
  if (result != VK_SUCCESS) fprintf(stderr, "error: framebuffer creation failed, %d\n", result);
}

void allocateCommandBuffers(
  VkCommandPool    commandPool,
  VkDevice         device,
  VkCommandBuffer *pCommandBuffers
  )
{
  VkCommandBufferAllocateInfo  commandBufferAllocateInfo = {0};
  VkResult                     result                    = {0};

  commandBufferAllocateInfo.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
//commandBufferAllocateInfo.pNext              = ;
  commandBufferAllocateInfo.commandPool        = commandPool;
  commandBufferAllocateInfo.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = COM_BUF_COUNT;

  result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, pCommandBuffers);
  if (result != VK_SUCCESS) fprintf(stderr, "error: allocate command buffers failed\n");
}

void createShaderModule(
  VkDevice        device,
  VkShaderModule *pVertexShaderModule
  )
{
  char                     *vertexCodeFilename     = "vert.spv";
  uint32_t                 *vertexCode             = {0};
  size_t                    vertexCodeSize         = {0};
  VkShaderModuleCreateInfo  vertexShaderModuleCreateInfo = {0};
  VkResult                  result                 = {0};

  readFile(vertexCodeFilename, &vertexCode, &vertexCodeSize);

  vertexShaderModuleCreateInfo.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  vertexShaderModuleCreateInfo.codeSize = vertexCodeSize;
  vertexShaderModuleCreateInfo.pCode    = vertexCode;

  result = vkCreateShaderModule(device, &vertexShaderModuleCreateInfo, 0, pVertexShaderModule);
  if (result != VK_SUCCESS) fprintf(stderr, "error: create vertex module failed\n");
}

void createPipelineLayout (
  VkDevice          device,
  VkPipelineLayout *pPipelineLayout
  )
{
  VkPipelineLayoutCreateInfo  pipelineLayoutCreateInfo = {0};

  pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

  vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, 0, pPipelineLayout);
}

void createSemaphore(
  VkDevice     device,
  VkSemaphore *pSemaphore
  )
{
  VkSemaphoreCreateInfo  semaphoreCreateInfo = {0};
  VkResult               result              = {0};

  semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

  result = vkCreateSemaphore(device, &semaphoreCreateInfo, 0, pSemaphore);
  if (result != VK_SUCCESS) fprintf(stderr, "error: couldn't create semaphore, %d\n", result);
}

void getSwapchainImages(
  VkDevice        device,
  VkSwapchainKHR  swapchain,
  VkImage        *pImages
  )
{
  uint32_t  swapchainImageCount = {0};

  swapchainImageCount = 1;

  vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, pImages);
}

void getQueueFamilyProperties(
  VkPhysicalDevice         physicalDevice,
  VkQueueFamilyProperties *pQueueFamilyProperties
  )
{
  uint32_t  queueFamilyPropertyCount = {0};

  queueFamilyPropertyCount = 1;

  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, pQueueFamilyProperties);
}

void getPhysicalDevice(
  VkInstance        instance,
  VkPhysicalDevice *pPhysicalDevices
  )
{
  uint32_t  physicalDeviceCount = PHY_DEV_COUNT;

  vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, 0);
  //printf("physical devices: %d\n", physicalDeviceCount);

  physicalDeviceCount = 1;

  vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, pPhysicalDevices);
}

void createWindowSurface(
  VkInstance    instance,
  GLFWwindow   *pWindow,
  VkSurfaceKHR *pSurface
  )
{
  VkResult  result = {0};

  result = glfwCreateWindowSurface(instance, pWindow, 0, pSurface);
  if (result != VK_SUCCESS) fprintf(stderr, "error: TODO, %d\n", result);
}

void getSurfaceCapabilities(
  VkPhysicalDevice          physicalDevice,
  VkSurfaceKHR              surface,
  VkSurfaceCapabilitiesKHR *pSurfaceCapabilities
  )
{
  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, pSurfaceCapabilities);
}

void getDeviceExtensionProperties(
  VkPhysicalDevice  physicalDevice
  )
{
  uint32_t              enumCount      = 256;
  VkExtensionProperties enumProps[256] = {0};

  vkEnumerateDeviceExtensionProperties(physicalDevice, 0, &enumCount, 0);

  enumCount = MIN(enumCount, 256);

  vkEnumerateDeviceExtensionProperties(physicalDevice, 0, &enumCount, enumProps);
  /*
  printf("enum count: %d\n", enumCount);
  for (int i = 0; i < enumCount; i++) {
    printf("supported: '%s'\n", enumProps[i].extensionName);
  }
  */
}

void getPhysicalDevicePresentationSupport(
  VkInstance        instance,
  VkPhysicalDevice  physicalDevice
  )
{
  int glfwPresentationSupport = glfwGetPhysicalDevicePresentationSupport(instance, physicalDevice, 0);

  // TODO "0"->"queueFamilyIndex"

  (void)glfwPresentationSupport;
  // printf("glfw presentation support: %d\n", glfwPresentationSupport);
}

void beginRenderPass(
  VkCommandBuffer  commandBuffer,
  VkRenderPass     renderPass,
  VkFramebuffer    framebuffer
  )
{
  VkRenderPassBeginInfo  renderPassBeginInfo = {0};
  VkClearValue           clearValue          = {{{0.0f, 0.0f, 0.0f, 1.0f}}};

  renderPassBeginInfo.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  renderPassBeginInfo.renderPass        = renderPass;
  renderPassBeginInfo.framebuffer       = framebuffer;
  renderPassBeginInfo.renderArea.offset = (VkOffset2D){0, 0};
  renderPassBeginInfo.renderArea.extent = (VkExtent2D){800, 600};  // TODO "swapchain"
  renderPassBeginInfo.clearValueCount   = 1;
  renderPassBeginInfo.pClearValues      = &clearValue;

  vkCmdBeginRenderPass(commandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void beginCommandBuffer(
  VkCommandBuffer  commandBuffer
  )
{
  VkCommandBufferBeginInfo  bufferBeginInfo = {0};
  VkResult                  result          = {0};

  bufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  result = vkBeginCommandBuffer(commandBuffer, &bufferBeginInfo);
  if (result != VK_SUCCESS) fprintf(stderr, "begin command buffer failed\n");
}

void getNextImage(
  VkDevice        device,
  VkSwapchainKHR  swapchain,
  VkSemaphore     semaphore,
  uint32_t       *pImageIndex
  )
{
  VkResult  result = {0};

  result = vkAcquireNextImageKHR(device, swapchain, TIMEOUT, semaphore, 0, pImageIndex);
  if (result != VK_SUCCESS) fprintf(stderr, "error: acq next image failed, %d\n", result);
}

void submitQueue(
  VkQueue          queue,
  VkCommandBuffer *pCommandBuffer
  )
{
  VkSubmitInfo  submitInfo = {0};
  VkResult      result     = {0};

  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = pCommandBuffer;

  result = vkQueueSubmit(queue, 1, &submitInfo, 0);  // TODO fence?
  if (result != VK_SUCCESS) fprintf(stderr, "error: queue submission failed, %d\n", result);
}

void presentQueue(
  VkQueue          queue,
  VkSwapchainKHR  *pSwapchains,
  uint32_t        *pImageIndices
  )
{
  VkPresentInfoKHR  presentInfo = {0};

  presentInfo.sType          = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains    = pSwapchains;
  presentInfo.pImageIndices  = pImageIndices;

  vkQueuePresentKHR(queue, &presentInfo);
}

void drawFrame(
  VkDevice         device,
  VkSwapchainKHR  *pSwapchain,
  VkCommandBuffer *pCommandBuffer,
  VkSemaphore      semaphore,
  VkRenderPass     renderPass,
  VkPipeline       pipeline,
  VkFramebuffer    framebuffer,
  VkQueue          queue
  // TODO pass all by reference?
  )
{
  uint32_t  imageIndex    = {0};
  uint32_t  vertexCount   = 3;
  uint32_t  instanceCount = 1;
  uint32_t  firstVertex   = {0};
  uint32_t  firstInstance = {0};

  getNextImage(device, *pSwapchain, semaphore, &imageIndex);
  // TODO vkResetCommandBuffer ?
  beginCommandBuffer(*pCommandBuffer);
  beginRenderPass(*pCommandBuffer, renderPass, framebuffer);
  vkCmdBindPipeline(*pCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
  // TODO vkCmdSetViewport ?
  // TODO vkCmdSetScissor ?
  vkCmdDraw(*pCommandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
  vkCmdEndRenderPass(*pCommandBuffer);
  vkEndCommandBuffer(*pCommandBuffer);
  submitQueue(queue, pCommandBuffer);
  presentQueue(queue, pSwapchain, &imageIndex);
}

int
main(void)
{
  GLFWwindow *              pWindow                        = {0};
  VkInstance                instance                       = {0};
  VkPhysicalDevice          physicalDevices[PHY_DEV_COUNT] = {0};
  VkSurfaceKHR              surface                        = {0};
  VkSurfaceCapabilitiesKHR  surfaceCapabilities            = {0};
  VkQueueFamilyProperties   queueFamilyProperties          = {0};
  VkDevice                  device                         = {0};
  VkQueue                   queue                          = {0};
  VkSwapchainKHR            swapchain                      = {0};
  VkImage                   image                          = {0};
  VkImageView               imageView                      = {0};
  VkRenderPass              renderPass                     = {0};
  VkShaderModule            vertexShaderModule             = {0};
  VkPipelineLayout          pipelineLayout                 = {0};
  VkPipeline                pipeline                       = {0};
  VkFramebuffer             framebuffer                    = {0};
  VkCommandPool             commandPool                    = {0};
  VkCommandBuffer           commandBuffer                  = {0};
  VkSemaphore               semaphore                      = {0};

  printf("hello draw\n");

  createWindow(&pWindow);
  createInstance(&instance);
  createDebugMessenger(instance);
  getPhysicalDevice(instance, physicalDevices);
  createWindowSurface(instance, pWindow, &surface);
  getSurfaceCapabilities(physicalDevices[0], surface, &surfaceCapabilities);
  getDeviceExtensionProperties(physicalDevices[0]);
  getPhysicalDevicePresentationSupport(instance, physicalDevices[0]);
  getQueueFamilyProperties(physicalDevices[0], &queueFamilyProperties);
  createLogicalDevice(physicalDevices[0], &device);
  vkGetDeviceQueue(device, 0, 0, &queue);
  createSwapchain(surface, surfaceCapabilities, device, &swapchain);
  getSwapchainImages(device, swapchain, &image);
  createImageView(image, device, &imageView);
  createRenderPass(device, &renderPass);
  createShaderModule(device, &vertexShaderModule);
  createPipelineLayout(device, &pipelineLayout);
  createGraphicsPipelines(vertexShaderModule, pipelineLayout, renderPass, device, &pipeline);
  createFramebuffer(renderPass, &imageView, device, &framebuffer);
  createCommandPool(device, &commandPool);
  allocateCommandBuffers(commandPool, device, &commandBuffer);
  createSemaphore(device, &semaphore);

  for (; !glfwWindowShouldClose(pWindow); ) {
    // TODO glfwPollEvents();
    drawFrame(device, &swapchain, &commandBuffer, semaphore, renderPass, pipeline, framebuffer, queue);
  }

  return EXIT_SUCCESS;
}
